import json
from random import randint
from flask import Flask, request, Response
app = Flask(__name__)

game = None
WIDTH = 0
HEIGHT = 1

#Game logic

class Cache(object):

	def write(self, txt):
		cache = open('mycache.txt', 'w')
		cache.write(txt)
		cache.close()

	def get(self):
		with open('mycache.txt', 'r') as cache:
			game = json.load(cache)
			return game

# ==================================================
# Utility
def cell_to_xy(cell):
	x = ord(cell[0]) - 64
	return [x,cell[1:]]

def xy_to_cell(xy):
	x = str(unichr(xy[0] + 64))
	y = ''.join(map(str, xy[1:]))
	return x + y

def alpha_to_decimal(alpha):
	return ord(alpha)-64

def random_cell(gridSize):
	dimensions = cell_to_xy(gridSize)

	return xy_to_cell([randint(0,int(dimensions[0])), randint(0,int(dimensions[1]))])

def get_a_surrounding_position(game,iteration):
	xy = game[u'last_successful']		
	x = int(xy[0])
	y = int(xy[1])	
	print "last_successful", x, y, iteration
	success_offsets = [ [0,-1], [-1,0], [1,0], [1,0] ]

	offset = success_offsets[iteration-1]

	new_x = x + offset[0]
	new_y = y + offset[1]
	print "HERE", new_x, new_y
	if new_x < 1:
		new_x = 1
	if new_x > alpha_to_decimal(game[u"gridSize"][0]):
		new_x = alpha_to_decimal(game[u"gridSize"][0])

	if new_y < 1:
		new_y = 1
	if new_y > int(game[u"gridSize"][1:]):
		new_y = int(game[u"gridSize"][1:])
	print new_x, new_y
	return [new_x, new_y]

def setup(game_data_json):
	game_data = json.loads(game_data_json)
	game_data[u'last_successful'] = ""
	game_data[u'last_go_hit'] = ""
	dimensions = cell_to_xy(game_data[u"gridSize"])
	game_data[u'grid'] = [["" for x in range(int(dimensions[WIDTH]))] for x in range(int(dimensions[HEIGHT]))]
	return game_data

# ====================================
# Web server
@app.route("/START", methods=['POST'])
def hello():
	print "GET /START", request.data
	game = request.data	
	game_data = setup(game)
	Cache().write(json.dumps(game_data))
	return ''

@app.route("/PLACE")
def get_place():	
	game = Cache().get()

	json_string = json.dumps({"gridReference" : random_cell(game[u"gridSize"]), "orientation" : "horizontal"})
	print "GET /PLACE", json_string
	return json_string

@app.route("/PLACE", methods=['POST'])
def post_place():
	print "POST /PLACE", request.data
	return ''

def get_attack_cell(griSize, grid, iter=0):
	cell = random_cell(griSize)
	xy = cell_to_xy(cell)
	if iter > 15:
		if grid[xy[0]][xy[1]] =='':
			return cell
		else:
			return get_attack_cell(griSize, grid, iter+1)
	else:
		return cell
@app.route('/MOVE')
def get_move():
 	game = Cache().get()

	# if we have a MISS? we can ignore that too?
	if game[u'last_successful'] != "":
		iteration = 0 
		new_is_a_miss = True

		while( new_is_a_miss and iteration <= 4 ):
			iteration += 1
			new_position = get_a_surrounding_position(game,iteration)
			print game[u'grid'][new_position[0]][new_position[1]]
			if game[u'grid'][new_position[0]][new_position[1]]!="MISS":
				new_is_a_miss = False
				game[u'last_successful'] = ''
				Cache().write(json.dumps(game))
				json_string = json.dumps({"type": "ATTACK", "gridReference" : xy_to_cell([new_position[0], new_position[1]]) })
				break

	elif randint(0, 6) == 1:
		json_string = json.dumps({"type": "MINE", "gridReference" : random_cell(game[u"gridSize"])})
	else:
		json_string = json.dumps({"type": "ATTACK", "gridReference" : get_attack_cell(game[u'gridSize'], game[u'grid'])})

	print "GET /MOVE", json_string
	return json_string

@app.route("/HIT", methods=['POST'])
def post_hit():
	print "POST /HIT", request.data
	result = json.loads(request.data)
	attacker = result[u'attacker']
	if attacker == u'piship':
		xy = cell_to_xy(result[u'gridReference'])
		x = int(xy[0])
		y = int(xy[1])
		game = Cache().get()
		game[u'grid'][x][y] = 'HIT'
		game[u'last_successful'] = [x, y]
		Cache().write(json.dumps(game))
	return ''

@app.route("/MISS", methods=['POST'])
def post_miss():
	print "POST /MISS", request.data
	result = json.loads(request.data)
	attacker = result[u'attacker']
	xy = cell_to_xy(result[u'gridReference'])
 	x = int(xy[0])
	y = int(xy[1])
	game = Cache().get()
	if attacker == u'piship':
		print x, y
		try:
			game[u'grid'][x][y] = 'MISS'
		except:
			pass
	else:
		game[u'last_go_hit'] = [x,y]	
	Cache().write(json.dumps(game))
	return ''

@app.route("/HIT_MINE", methods=['POST'])
def post_mine():
	print "POST /HIT_MINE", request.data
	result = json.loads(request.data)
	attacker = result[u'attacker']
	if attacker == u'piship':
		xy = cell_to_xy(result[u'gridReference'])
		x = int(xy[0])
		y = int(xy[1])
		game = Cache().get()
		try:
			game[u'grid'][x][y] = 'MINE'
		except:
			pass
		Cache().write(json.dumps(game))
	return ''

@app.route("/SCAN", methods=['POST'])
def post_scan():
	print "POST /SCAN" , request.data
	result = json.loads(request.data)
	grid_refs = result[u'gridReferences']
	for grid_ref in grid_refs:
		xy = cell_to_xy(grid_ref)
		x = int(xy[0])
		y = int(xy[1])
		game = Cache().get()
		game[u'grid'][x][y] = 'MINE'
  	Cache().write(json.dumps(game))
	return ''

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=80, debug=True)

